/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stackbuilder.picoplacapredictor;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Santiago
 */
public class PicoPlacaPredictorTest {
    
    public PicoPlacaPredictorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void vehiculeIsAuthorizedToBeOnTheRoad(){        
        PicoPlaca picoPlacaTest = new PicoPlaca();
        picoPlacaTest.validateVehicleCirculation("PBL8776", "2017-07-04", "08:40");    
    }   
    
    @Test
    public void vehiculeIsNotAuthorizedToBeOnTheRoad(){        
        PicoPlaca picoPlacaTest = new PicoPlaca();
        picoPlacaTest.validateVehicleCirculation("PBL8776", "2017-07-05", "16:13");    
    }
    
    @Test
    public void plateNumberIsNotValid()
    {
      PicoPlaca picoPlacaTest = new PicoPlaca();
      assertFalse(picoPlacaTest.validatePlate("P3LP756"));
    }
    
    @Test
    public void inputDateIsValid()
    {
      PicoPlaca picoPlacaTest = new PicoPlaca();
      assertTrue(picoPlacaTest.validateDate("2017-08-21"));
    }
    
    
}
