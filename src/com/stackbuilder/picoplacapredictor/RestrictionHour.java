/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stackbuilder.picoplacapredictor;

import java.sql.Time;

/**
 *
 * @author Santiago
 */
public class RestrictionHour {
    private Time startHour;
    private Time endHour;    
    
    public RestrictionHour(Time startHour, Time endHour){
        this.startHour = startHour;
        this.endHour = endHour;
    }
    
    public void setStartHour(Time startHour) {
        this.startHour = startHour;
    }

    public void setEndHour(Time endHour) {
        this.endHour = endHour;
    }

    public Time getStartHour() {
        return startHour;
    }

    public Time getEndHour() {
        return endHour;
    }
}
