/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stackbuilder.picoplacapredictor;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Santiago
 */
public class PicoPlaca {
    private RestrictionHour restrictionHourDay;
    private RestrictionHour restrictionHourNight;
    private Map<Integer, RestrictionDay> restrictionDayList;

    /*Initialize all the Pico & Placa restrictions*/
    public PicoPlaca() {
        try{            
            /*Create the hour restrictions from 07:00 to 09:30 and 16:00 to 19:30*/
            SimpleDateFormat formatterHour = new SimpleDateFormat("HH:mm");
            Time startHourDay = new Time( formatterHour.parse("06:59").getTime() );
            Time endHourDay = new Time( formatterHour.parse("09:30").getTime() );
            restrictionHourDay = new RestrictionHour(startHourDay, endHourDay);
            Time startHourNight = new Time( formatterHour.parse("15:59").getTime() );
            Time endHourNight = new Time(formatterHour.parse("19:30").getTime() );
            restrictionHourNight = new RestrictionHour(startHourNight, endHourNight);
            
            /*Create the last digit plate restriction based on the day*/
            List<Integer> mondayRestrictionLastDigitList = new ArrayList();
            mondayRestrictionLastDigitList.add(1);
            mondayRestrictionLastDigitList.add(2);
            List<Integer> tuesdayRestrictionLastDigitList = new ArrayList();
            tuesdayRestrictionLastDigitList.add(3);
            tuesdayRestrictionLastDigitList.add(4);
            List<Integer> wednesdayRestrictionLastDigitList = new ArrayList();
            wednesdayRestrictionLastDigitList.add(5);
            wednesdayRestrictionLastDigitList.add(6);
            List<Integer> thursdayRestrictionLastDigitList = new ArrayList();
            thursdayRestrictionLastDigitList.add(7);
            thursdayRestrictionLastDigitList.add(8);
            List<Integer> fridayRestrictionLastDigitList = new ArrayList();
            fridayRestrictionLastDigitList.add(9);
            fridayRestrictionLastDigitList.add(0);

            RestrictionDay mondayRestriction = new RestrictionDay(2, mondayRestrictionLastDigitList);
            RestrictionDay tuesdayRestriction = new RestrictionDay(3, tuesdayRestrictionLastDigitList);
            RestrictionDay wednesdayRestriction = new RestrictionDay(4, wednesdayRestrictionLastDigitList);
            RestrictionDay thursdayRestriction = new RestrictionDay(5, thursdayRestrictionLastDigitList);
            RestrictionDay fridayRestriction = new RestrictionDay(6, fridayRestrictionLastDigitList);
            
            restrictionDayList = new HashMap();
            restrictionDayList.put(mondayRestriction.getDay(), mondayRestriction);
            restrictionDayList.put(tuesdayRestriction.getDay(), tuesdayRestriction);
            restrictionDayList.put(wednesdayRestriction.getDay(), wednesdayRestriction);
            restrictionDayList.put(thursdayRestriction.getDay(), thursdayRestriction);
            restrictionDayList.put(fridayRestriction.getDay(), fridayRestriction);
        }catch(Exception ex){
            System.out.println("An error occured: " + ex);
        }
    }
    
    /*Function to validate if a car is authorized or not to be on the road*/
    public void validateVehicleCirculation(String plateNumber,String date,String hour){
        try{
            if(validatePlate(plateNumber) && validateDate(date) && validateHour(hour)){
                boolean isAuthorized = true;
                SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat formatterHour = new SimpleDateFormat("HH:mm");
                Date inputDate = formatterDate.parse(date);
                Date inputHour = formatterHour.parse(hour);
                Calendar cal = Calendar.getInstance();
                cal.setTime(inputDate);
                int plateLastDigit = Integer.parseInt(plateNumber.charAt(plateNumber.length()-1)+"" );
                if((inputHour.after(restrictionHourDay.getStartHour()) && 
                        inputHour.before(restrictionHourDay.getEndHour()) || 
                        (inputHour.after(restrictionHourNight.getStartHour()) && 
                        inputHour.before(restrictionHourNight.getEndHour())
                        ))){
                    RestrictionDay lastDigitsBannedList = restrictionDayList.get(cal.get(Calendar.DAY_OF_WEEK));
                    for (Integer day : restrictionDayList.keySet()) {
                        if(day == cal.get(Calendar.DAY_OF_WEEK)){
                            for(Integer lastDigitBanned : lastDigitsBannedList.getLastDigitPlateNumber()){
                                if(lastDigitBanned == plateLastDigit){
                                    isAuthorized = false;
                                }
                            }
                        }
                    }
                }                
                
                String answer = "The vehicle with plate number " + plateNumber;            
                if(isAuthorized)
                    answer += " IS AUTHORIZED ";
                else
                    answer += " IS NOT AUTHORIZED ";
                answer += "to be on the road on " + date + " at " + hour;
                System.out.println(answer);
            }
            
            
        }catch(Exception e){
            System.out.println("An error ocurred: " + e);
        }
    }
    
    public boolean validatePlate(String plate){
        return !(plate.length()<6 || plate.length()>7 || !plate.matches("[a-zA-Z0-9]*") ||
                !plate.substring(0,3).matches("[a-zA-Z]*") ||
                !plate.substring(3).matches("[0-9]*"));
    }
    
    public boolean validateDate(String date){
        if(date.length() == 10 && 
                date.matches("([0-9]{4})-([0-9]{2})-([0-9]{2})") &&
                Integer.parseInt(date.substring(5,7))<=12 &&
                Integer.parseInt(date.substring(5,7))>0 &&
                Integer.parseInt(date.substring(8))<=31 &&
                Integer.parseInt(date.substring(8))>0) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean validateHour(String hour){
        if(hour.length() != 5 || !hour.matches("([01]?[0-9]|2[0-3]):[0-5][0-9]")) {
            return false;
        } else {
            return true;
        }
    }
}

