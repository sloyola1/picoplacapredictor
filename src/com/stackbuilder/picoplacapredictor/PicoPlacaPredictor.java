/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stackbuilder.picoplacapredictor;

import java.util.Scanner;

/**
 *
 * @author Santiago
 */
public class PicoPlacaPredictor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {        
        PicoPlaca picoplaca = new PicoPlaca();
        Scanner input = new Scanner(System.in);
        String plateNumber;
        String date;
        String hour;
        do{
            System.out.println("Please enter a valid plate number (PBR9478): ");
            plateNumber = input.next();
        }while(!picoplaca.validatePlate(plateNumber));
        do{
            System.out.println("Please enter a valid date (yyyy-mm-dd): ");
            date = input.next();
        }while(!picoplaca.validateDate(date));
        do{
            System.out.println("Please enter a valid hour (HH-mm): ");
            hour = input.next();
        }while(!picoplaca.validateHour(hour));    
          
        picoplaca.validateVehicleCirculation(plateNumber, date, hour);
    }
    
}
