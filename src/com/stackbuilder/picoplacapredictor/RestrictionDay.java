/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stackbuilder.picoplacapredictor;

import java.util.List;

/**
 *
 * @author Santiago
 */
public class RestrictionDay {
    private int day;
    private List<Integer> lastDigitPlateNumber;

    public RestrictionDay() {
    }

    public RestrictionDay(int day, List<Integer> lastDigitPlateNumber) {
        this.day = day;
        this.lastDigitPlateNumber = lastDigitPlateNumber;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setLastDigitPlateNumber(List<Integer> lastDigitPlateNumber) {
        this.lastDigitPlateNumber = lastDigitPlateNumber;
    }

    public int getDay() {
        return day;
    }

    public List<Integer> getLastDigitPlateNumber() {
        return lastDigitPlateNumber;
    }    
}

