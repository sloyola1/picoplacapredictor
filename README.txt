Pico & placa predictor, is an application that uses a plate number, a date and 
a time, to check if a car is authorized or not to be on the road.

2 classes are defined to manage pico & placa restrictions:

* RestrictionDay
* RestricionHour

Class PicoPlaca is used to initialize restrictions, validate inputs and validate
if a car is authorized or not to be on the road.

Inputs are validated as following:

1) Plate must start by 3 characters (only letter), followed by 3 o 4 numbers. Special characters are not allowed
2) Date must be in this format: yyyy-mm-dd
3) Time must be in this format: HH:mm


PicoPlacaPredictor is the main class and where the program starts.

PicoPlacaPredictorTest runs 4 test, for validating plate, date and wheter a car
is authorized or not.


